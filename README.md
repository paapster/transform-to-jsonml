# transform-to-jsonML


to run:

$ node text-to-json text json

text-to-json <input> <output>


TODO: Validate most enriched MD file avialable https://markdown-it.github.io/ & https://github.com/markdown-it/markdown-it
TODO: Improve texts --> https://github.com/sheremetyev/texts.js
TODO: Validate HASKELL textJSON transformer --> https://hackage.haskell.org/package/json-0.9.1/docs/Text-JSON.html
TODO: Check headless CMS schemas --> https://docs.getdirectus.com/6.4.0/#Schema_Guide
