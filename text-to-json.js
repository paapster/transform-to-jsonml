var parsedFrom, parsedTo, parsedToMongoDB;

process.argv.forEach(function(val, index, array) {
 	if (index === 2) {
		parsedFrom = val;
	}
	if (index === 3) {
		parsedTo = val;
	}
});


var fs = require('fs');
var walkdir = require('walkdir');
var path = require('path')


// Check input format
if ( ['text', 'json'].indexOf(parsedFrom) === -1 ) {
	console.log ('Source must be "json" or "text" files.');
	process.exit(1);
}
else {
	var reader = require('./node_modules/texts/lib/reader/' + parsedFrom);
}

// Check output format
if ( ['text', 'json', 'html5'].indexOf(parsedTo) === -1 ) {
	console.log ('Destination must be "json", "text" or "html5" files.');
	process.exit(1);
}
else {
	var writer = require('./node_modules/texts/lib/writer/' + parsedTo);
}

// Set couple of texts.js options
var parsedStandalone = parsedStandalone || false;
var parsedWrap = (typeof parsedWrap !== 'undefined') ? parsedWrap : true;
var writerOptions = { standalone: parsedStandalone, wrap: parsedWrap };

// Walk thru folder
var folder = path.join(__dirname + '/textFiles');
var files = walkdir.sync(folder);


// Determine if there are correct files.
files = files.filter(function(file) { return file.substr(-5) === '.' + parsedFrom });
files = files.filter(function(file) { return fs.statSync(file).isFile(); });

// Check if there are source files to transform
if (files.length === 0) {
	console.log ('There are no ".' + parsedFrom +'" files to transform');
}
else {
	files.forEach(function (file) {
		console.log('Orginal: ' + file);
		var input = fs.readFileSync(file, 'utf8');
		var text = reader(input);
		var output = writer(text, writerOptions);

		var templateName = path.basename(file, '.' + parsedFrom);
		var newFile = path.join(folder, templateName + '.' + parsedTo);

		fs.writeFile(newFile, output, function (error) {
			if (error) throw error;
			console.log('New: ' + newFile);
		});

	});
}